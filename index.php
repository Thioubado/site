<?php

    class Employee  extends Person{

        public $jobTitle;

        public function __construct($jobTitle, $firstName, $lastName, $gender ='f')
        {
            $this->jobTitle = $jobTitle;

            parent::__construct($firstName, $lastName, $gender);
        }

        public function getJobTitle()
        {
            return $this->jobTitle;
        }
    }

    class Person {

        public $firstName;
        public $lastName;
        public $gender;


        public function __construct($firstName, $lastName, $gender = 'f')
        {
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->gender = $gender;
        }

        public function sayHello()
        {
            return "Hello my firstname is $this->firstName and my name is $this->lastName"; 
        }

    }




$tom = new Person('Tom', 'Ben', 'm');
$jane = new Person('Jane', 'Sylla');

$momo = new Employee("Backend developper", "Momo", "Seck");

/*echo $tom->firstName. '<br>';
echo $tom->lastName. '<br>';
echo $tom->gender. '<br>';
echo $tom->sayHello(). '<br>';
echo $jane->sayHello(). '<br>';*/

echo $momo->firstName. '<br>';
echo $momo->lastName. '<br>';
echo $momo->jobTitle. '<br>';
echo $momo->getJobTitle(). '<br>';
echo $momo->sayHello(). " and i am a $momo->jobTitle". '<br>';
echo $momo->getJobTitle();
